-- TVStext SQL setup script
-- Requires MySQL 5.x.x

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `routes` (
  `name` varchar(16384) NOT NULL,
  `number` varchar(16384) NOT NULL,
  `tvsnumber` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `usage` (
  `address` varchar(2048) NOT NULL,
  `emailbody` varchar(16384) NOT NULL,
  `route` varchar(16384) NOT NULL,
  `response` varchar(16384) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
