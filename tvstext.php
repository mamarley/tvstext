#!/usr/bin/php
<?php
/*  tvstext.php - Provides SMS access to the Transloc TVS system.
    Copyright (C) 2011 Michael Marley <michael@michaelmarley.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "simple_html_dom.php";
require_once "class.phpmailer.php";
$tvstextversion="3.0.7";

function updatedb($ncsutranslocip,$mysql_connection){
	$currentroutes=mysql_query("SELECT * FROM routes",$mysql_connection);
	$numcurrentroutes=mysql_numrows($currentroutes);
	for($i=0;$i<$numcurrentroutes;$i++){
		$name=mysql_result($currentroutes,$i,"name");
		$number=mysql_result($currentroutes,$i,"number");
		$tvsnumber=mysql_result($currentroutes,$i,"tvsnumber");
		$html=file_get_html("http://$ncsutranslocip/t/route.php?id=$tvsnumber");
		$ret=$html->find('h1');
		if(stristr($ret[0],"route not found")){
			mysql_query("DELETE FROM routes WHERE name='$name' AND number='$number' AND tvsnumber=$tvsnumber",$mysql_connection);
		}
	}
	$html=file_get_html("http://$ncsutranslocip/t/");
	$ret=$html->find('a');
	array_pop($ret);array_pop($ret);
	$arraylength=count($ret);
	for($i=0;$i<$arraylength;$i++){
		$tvsnumber=(int)str_replace("route.php?id=","",$ret[$i]->href);
		$rawlocalrouteno=mysql_query("SELECT * FROM routes WHERE tvsnumber=$tvsnumber",$mysql_connection);
		$route=$ret[$i]->plaintext;
		if(stristr($route,"red terror")){
			$tvstextaccessname="rt";
			$name=$route;
			$srnumber=NULL;
		}else if(stristr($route,"holiday")){
			$tvstextaccessname="hs";
			$name=$route;
			$srnumber=NULL;
		}else{
			$route=explode(" Rt ",$route);
			$name=$route[0];
			$tvstextaccessname=strtolower($route[1]);
			$srnumber=$route[1];
			if(stristr($name,"tripper")||stristr($name,"express")){
				if($tvstextaccessname[strlen($tvstextaccessname)-1]!='a'){
					$tvstextaccessname=$tvstextaccessname . 'a';
				}
			}
			if(stristr($name,"limited")){
				$tvstextaccessname="ls" . $tvstextaccessname;
			}
		}
		$updateroutecheck=mysql_query("SELECT * FROM routes WHERE number='$tvstextaccessname'",$mysql_connection);
		$updatenumrows=mysql_numrows($updateroutecheck);
		$addroutecheck=mysql_query("SELECT * FROM routes WHERE name='$name' AND number='$tvstextaccessname' AND tvsnumber=$tvsnumber",$mysql_connection);
		$addnumrows=mysql_numrows($addroutecheck);
		if(($updatenumrows>0)&&($addnumrows==0)){
			mysql_query("UPDATE `routes` SET `name`='$name' WHERE number='$tvstextaccessname'");
			mysql_query("UPDATE `routes` SET `tvsnumber`='$tvsnumber' WHERE number='$tvstextaccessname'");
		}else{
			if($addnumrows==0){
				mysql_query("INSERT INTO `routes` (name,number,tvsnumber) VALUES ('$name','$tvstextaccessname','$tvsnumber')",$mysql_connection);
			}
		}
	}
}

do{
	$ncsutranslocip=gethostbyname("ncsu.transloc.com");
}while(($ncsutranslocip=="ncsu.transloc.com")&&(sleep(2)||TRUE));
while(1){
	$previoustime=time();
	if(!mysql_ping($mysql_connection)){
		mysql_close($mysql_connection);
		do{
			$mysql_connection=mysql_connect('localhost','username','password');
		}while((!$mysql_connection)&&(sleep(2)||TRUE));
		mysql_select_db('tvstext',$mysql_connection);
	}
	if(!imap_ping($imap_connection)){
		imap_close($imap_connection);
		do{
			$imap_connection=imap_open("{localhost:143/notls}Inbox","tvstext","password");
		}while((!$imap_connection)&&(sleep(2)||TRUE));
	}
	$crontimer++;
	if($crontimer>=1800){
		mysql_query("DELETE FROM `usage` WHERE time < DATE_SUB(NOW(), INTERVAL 30 DAY)",$mysql_connection);
		mysql_query("OPTIMIZE TABLE `usage`",$mysql_connection);
		if(($ncsutranslociptemp=gethostbyname("ncsu.transloc.com"))!="ncsu.transloc.com"){
			$ncsutranslocip=$ncsutranslociptemp;
		}
		$crontimer=0;
	}
	$count=imap_num_msg($imap_connection);
	for($i=1;$i<=$count;$i++) {
		$header=imap_headerinfo($imap_connection,$i);
		$sender=$header->reply_toaddress;
		if(stristr($header->subject,"returned")||stristr($sender,"tvstext")){
			imap_delete($imap_connection,$i);
			continue;
		}
		$raw_body=imap_body($imap_connection,$i);
		$sender_sql=mysql_real_escape_string($sender);
		$raw_body_sql=mysql_real_escape_string($raw_body);
		if(stristr($sender,'<')&&stristr($sender,'>')){
			$sender=substr($sender,strpos($sender,'<')+1,strpos($sender,'>')-(strpos($sender,'<')+1));
		}
		$body=str_replace("route","",strtolower(trim(preg_replace("/[^a-zA-Z0-9]/","",$raw_body))));
		if(stristr($sender,"att")){
			$body=rtrim(str_replace("thismobiletextmessageisbroughttoyoubyatt","",$body));
		}
		if(stristr($sender,"mms")||stristr($sender,"pix")){
			$message="MMS messages not supported!  Please use SMS only!";
		}else if(stristr($raw_body,"<html>")||stristr($raw_body,"This is a multi-part message in MIME format.")){
			$message="HTML/multipart messages not supported!  Please use plaintext only!";
		}else if($body=="list"){
			$html=file_get_html("http://$ncsutranslocip/t/");
			$ret=$html->find('a');
			array_pop($ret);array_pop($ret);
			$arraylength=count($ret);
			if($arraylength==0){
				$message="No routes in service!";
			}else{
				$message="Routes in service:\r";
				for($j=0;$j<$arraylength;$j++){
					$tvsnumber=(int)str_replace("route.php?id=","",$ret[$j]->href);
					$rawlocalrouteno=mysql_query("SELECT * FROM routes WHERE tvsnumber=$tvsnumber",$mysql_connection);
					if(mysql_numrows($rawlocalrouteno)==0){
						updatedb($ncsutranslocip,$mysql_connection);
						$rawlocalrouteno=mysql_query("SELECT * FROM routes WHERE tvsnumber=$tvsnumber",$mysql_connection);
					}
					$localrouteno=mysql_result($rawlocalrouteno,0,"number");
					$localroutename=mysql_result($rawlocalrouteno,0,"name");
					$message.=$localroutename . " (" . $localrouteno . ")";
					if($arraylength-$j>1){
						$message.="\n";
					}
				}
			}
		}else if($body=="help"){
			$message="Text a route number (ie. 3) and I will text back the location of the bus(es) on that route.";
		}else if($body=="info"){
			$phpmailerversion=new PHPMailer();
			$phpmailerversion=$phpmailerversion->Version;
			$dovecot_version=explode("=",shell_exec("/usr/sbin/postconf mail_version"));
			$dovecot_version=$dovecot_version[1];
			$message="TVStext $tvstextversion" . "\nLinux " . trim(shell_exec("uname -r")) . "\nPHP " . phpversion() . "\nMySQL " . mysql_get_server_info() . "\nPostfix " . trim($dovecot_version) . "\nDovecot " . trim(shell_exec("/usr/sbin/dovecot --version")) . "\nPHPMailer $phpmailerversion" . "\nLoad Avg: " . substr(file_get_contents('/proc/loadavg'),0,15);
		}else{
			$routes=mysql_query("SELECT * FROM routes WHERE number='$body'",$mysql_connection);
			if(mysql_numrows($routes)==0){
				updatedb($ncsutranslocip,$mysql_connection);
				$routes=mysql_query("SELECT * FROM routes WHERE number='$body'",$mysql_connection);
			}
			if(strlen($body)<=2){
					$message="Route " . $body . "\n";
				}else{
					$message=$body . "\n";
				}
			if(mysql_numrows($routes)==0){
				$message.="Invalid route!";
			}else{
				$name=mysql_result($routes,0,"name");
				$tvsnumber=mysql_result($routes,0,"tvsnumber");
				$html=file_get_html("http://$ncsutranslocip/t/route.php?id=$tvsnumber");
				$ret=$html->find('li');
				array_pop($ret);array_pop($ret);
				$arraylength=count($ret);
				if($arraylength==0){
					updatedb($ncsutranslocip,$mysql_connection);
					$routes=mysql_query("SELECT * FROM routes WHERE number='$body'",$mysql_connection);
					$name=mysql_result($routes,0,"name");
					$tvsnumber=mysql_result($routes,0,"tvsnumber");
					$html=file_get_html("http://$ncsutranslocip/t/route.php?id=$tvsnumber");
					$ret=$html->find('li');
					array_pop($ret);array_pop($ret);
					$arraylength=count($ret);
				}
				if(mysql_numrows($routes)==0){
					$message.="Invalid route!";
				}else if($arraylength==0){
					$message.="Route out of service or no buses in service on this route.";
				}else{
					for($j=0;$j<$arraylength;$j++){
						$bus=$ret[$j]->find('div');
						$ret[$j]->find('div',0)->innertext='';
						$ret[$j]=strip_tags($ret[$j],'<br>');
						$ret[$j]=explode('<br />',$ret[$j]);
						$array2length=count($ret[$j]);
						$message.="Bus ";
						$message.=$j+1 . "\n";
						for($k=0;$k<$array2length;$k++){
							$message.=$ret[$j][$k] . "\n";
						}
					}
				}
			}
		}
		imap_delete($imap_connection,$i);
		$message=html_entity_decode($message);
		mysql_query("INSERT INTO `usage` (address,emailbody,route,response,time) VALUES ('$sender_sql','$raw_body_sql','$body','$message',NOW())",$mysql_connection);
		$message=str_split($message,154);
		$numberofparts=count($message);
		for($j=0;$j<$numberofparts;$j++){
			$mail=new PHPMailer();
			$mail->IsSMTP();
			$mail->Host="localhost";
			$mail->SMTPAuth=true;
			$mail->Port=25;
			$mail->Username="username";
			$mail->Password="password";
			$mail->AddAddress($sender);
			$mail->SetFrom('tvstext@example.com');
			$mail->Body=$message[$j];
			$mail->Send();
		}
	}
	imap_expunge($imap_connection);
	$timedifference=time()-$previoustime;
	if($timedifference<2){
		sleep(2-$timedifference);
	}
}
?>
